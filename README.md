# `fontite` - pack icon fonts tightly!

![`fontite` logo](./fontite.svg)

Trim and combine multiple icon fonts to contain only the glyphs you need.
Easy way to cut down on the size of your website, making it faster for your users to load and cheaper for you to host.

Typical icon fonts with their associated CSS are on the order of hundreds of kilobytes.
With `fontite`, you might be able to fit it all in under 10 kB.

If you're using multiple icon fonts, or any external SVG icons, `fontite` can pack them into just one font and CSS file, reducing the overall number of network requests your users have to make.

`fontite` has support for WOFF2, WOFF, OTF, TTF, SVG, and EOT font formats, both as inputs and outputs. It can also import icons from standalone SVG, EPS, and GLIF files.

`fontite` works without any JavaScript on your website as a pure HTML/CSS solution.

## How to use

### Dependencies

You'll need Python 3.10, [Tomli](https://pypi.org/project/tomli/), and [FontForge](https://fontforge.org/) installed.

### Configuration

`fontite` uses [TOML](https://toml.io) as a configuration language.
The [example config](./fontite.toml) in this repository is well-documented and should be used as a starter configuration.

You can create a new configuration file for each customized icon font you need, and check them in with your sites' version control.

### Collecting inputs

The `[sources.*]` and `[settings.base-settings-font]` sections of `fontite.toml` specify paths to input font and glyph files.

For a font file, you can use any font format supported by FontForge - WOFF2, WOFF, OTF, TTF, etc.

For a glyph file, you can use any glyph format supported by FontForge.
You'd probably want to use an SVG file.

### Running

When run without any arguments, `fontite` will look for a `fontite.toml` in the current working directory.

```
./fontite
```

The optional first argument can specify an explicit path to a configuration file.

```
./fontite path/to/fontite.toml
```

When run, it will create all of the output font files specified in `[outputs.fonts]` of the configuration file, as well as optionally a CSS file as described by `[outputs.css]`.

#### Font output

Your selected icons will be placed in a continuous section of the output font starting from `\F000`.
You can view these in FontForge and use the icons in your UI by including their Unicode codepoints.

```html
<span>\F000</span>
<span>\F001</span>
<!-- ... -->
```

#### CSS output

The CSS output allows icons to be accessed by name when used on the web.

Icon class names correspond to names in the config.
The [example config](./fontite.toml) allows you to include icons like this:

```html
<!-- From the `fa-solid` source font -->
<span class="icon icon-code-branch"></span>
<!-- From the `openwebicons` source font, demonstrating a renamed icon -->
<span class="icon icon-share"></span>
<!-- From the `matrix-org` source glyph SVG -->
<span class="icon icon-matrix-org"></span>
```

The global icon class name and per-icon class prefix are customizable.
The output path and relative URLs to font file locations can also be customized.
The output CSS can easily be included in a SASS, SCSS, or LESS build system if desired.

## Demo page

There is a full demo of a `fontite` setup for a real webpage in the [demo](./demo) directory.

## Comparison with alternatives

These options are listed on the [FontAwesome wiki](https://github.com/FortAwesome/Font-Awesome/wiki/Customize-Font-Awesome):

- [Fontello](http://fontello.com/)
- [IcoMoon](https://icomoon.io/app/#/select)
- [Fontastic](http://fontastic.me/)

All of these are online services (some requiring an account to use) which aren't really a good fit for an automated pipeline.

Both Fontello and [icnfnt](https://github.com/johnsmclay/icnfnt) are open source, but both use a full client/server architecture and a browser-based editor UI.
They are also quite large projects, particularly Fontello which has over 65000 lines of JavaScript code (not including dependencies!).

On the other hand, `fontite` is intended as an extremely minimal CLI-based interface around the FontForge API, for developers who already know exactly what they need out of their icon font. It fits neatly into a static site generator pipeline, and integrates nicely with version control.

## Contributing

Issue reports and MRs are welcome!
